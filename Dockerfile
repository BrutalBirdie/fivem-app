FROM cloudron/base:3.0.0@sha256:455c70428723e3a823198c57472785437eb6eab082e79b3ff04ea584faf46e92

RUN mkdir -p /app/code
WORKDIR /app/code


RUN wget https://runtime.fivem.net/artifacts/fivem/build_proot_linux/master/4394-572b000db3f5a323039e0915dac64641d1db408e/fx.tar.xz \
    && tar xvf fx.tar.xz \
    && git clone https://github.com/citizenfx/cfx-server-data.git ./server-data-default \
    && ln -s /app/data/server-data server-data \
    && rm -rf fx.tar.xz \
    && ln -s /app/data/txData/ txData

COPY server.cfg /app/code/server-data-default/
COPY start.sh /app/code/


CMD [ "/app/code/start.sh" ]
