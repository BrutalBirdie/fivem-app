### Overview

This app packages the FiveM Server <upstream>3184</upstream>

FiveM is a modification for Grand Theft Auto V enabling you to play multiplayer on customized dedicated servers, powered by Cfx.re.

FiveM is not affiliated with or endorsed by Rockstar North,
Take-Two Interactive or other rightsholders.
Any trademarks used belong to their respective owners.
