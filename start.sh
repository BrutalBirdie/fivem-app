#!/bin/bash

# Cloudron edit
if [ ! -d "/app/data/server-data" ]; then
    cp -r /app/code/server-data-default /app/data/server-data
fi

if [ ! -d "/app/data/txData" ]; then
    mkdir /app/data/txData
fi

sed -i -e "s/set mysql_connection_string .*/# set mysql_connection_string \"server=$CLOUDRON_MYSQL_HOST;database=$CLOUDRON_MYSQL_DATABASE;userid=$CLOUDRON_MYSQL_USERNAME;password=$CLOUDRON_MYSQL_PASSWORD\"/g" /app/data/server-data/server.cfg

cd /app/code/server-data/

# save the script directory
SCRIPT=$(readlink -f "$0")
SCRIPTPATH="/app/code/"

# run server
exec $SCRIPTPATH/alpine/opt/cfx-server/ld-musl-x86_64.so.1 \
    --library-path "$SCRIPTPATH/alpine/usr/lib/v8/:$SCRIPTPATH/alpine/lib/:$SCRIPTPATH/alpine/usr/lib/" -- \
    $SCRIPTPATH/alpine/opt/cfx-server/FXServer +set citizen_dir $SCRIPTPATH/alpine/opt/cfx-server/citizen/ $*
